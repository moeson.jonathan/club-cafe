Voici le rendu de mon projet Club-Café, Il va falloir suivre les etapes afin d'installer et configurer correctement le projet 

1- Faire la commande : 
	git clone git@gitlab.com:moeson.jonathan/club-cafe.git
	ou 
	git clone https://gitlab.com/moeson.jonathan/club-cafe.git
2-Importer la base donnée qui se trouve: club-cafe/club_cafe.sql
	
3-Se Deplacer a l'aide de la commande :
	cd club-café/
4-Copier le contenue du .envExemple et remplacer avec vos identifiants

5- Lancer la commande :
	Composer up
	npm i
	
6- Pour finir lancer la commande :
	symfony server:start
7- Cliquer sur l'adresse http : 
	 [OK] Web server listening                                                  
	      The Web server is using PHP CLI 8.1.2                                 
	      http://127.0.0.1:8001   
