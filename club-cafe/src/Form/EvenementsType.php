<?php

namespace App\Form;

use App\Entity\Evenements;
use Symfony\Component\Form\AbstractType;
use Doctrine\Common\Collections\Expr\Value;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\Range;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;


class EvenementsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('nom')
            ->add('date', DateType::class, [
                'label' => 'Date',
                'widget' => 'single_text',
                'format' => 'yyyy-MM-dd',
                'data' => new \DateTime(date('Y-m-d')), 
                'attr' => [
                    'min' => date('Y-m-d'), // Date minimum autorisée
                    'max' => date('Y-m-d', strtotime('+1 month')),
                ],
                'constraints' => [
                    new Range([
                        'min' => 'today',
                        'max' => '+30 days',
                        'minMessage' => 'La date ne doit pas être antérieure à aujourd\'hui.',
                        'maxMessage' => 'La date ne doit pas être postérieure à {{ limit }} jours à partir d\'aujourd\'hui.',
                    ]),
                ],
            ])
            ->add('description', TextareaType::class, [
                'attr' => ['class' => 'tinymce'],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Evenements::class,
        ]);
    }
}
